<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referencia extends Model
{
        protected $table = 'referencia';
       protected $fillable = [
        'institucion_id', 'estado', 'direccion','latitud','longitud','responsable','horario','telefonos','revision'
    ];

}
