<?php

namespace App\Http\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Beneficiario;
use App\Sugerencia;
use DB;

class SugerenciasController extends Controller
{
    public function store(Request $request){

        /*
        |-----------------------------------------------------------------
        |  Generar Folio
        |----------------------------------------------------------
        */
        $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $numerodeletras=10;
        $folio = "";
        for($i=0;$i<$numerodeletras;$i++)
        {
            $folio .= substr($caracteres,rand(0,strlen($caracteres)),1);
        }

        $beneficiario = new Beneficiario;
        $beneficiario->folio = $folio;
        $beneficiario->nombre = $request->nombre;
        $beneficiario->telefono = $request->telefono;
        $beneficiario->email = $request->email;
        $beneficiario->entidadFederativa_id = '9';
        $beneficiario->save();

        $ciudadano = DB::table('ciudadanos')->where('folio', $folio)->value('id');

        if($ciudadano){
            $sugerencia = new Sugerencia;
            $sugerencia->sugerencia = $request->sugerencia;
            $sugerencia->ciudadanos_id = $ciudadano;
            $sugerencia->save();
        }

        return redirect()->back()->with('message', 'Sugerencia enviada');
    }

}