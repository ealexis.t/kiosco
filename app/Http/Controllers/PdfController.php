<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Beneficiario;
use App\Implicados;
use DB;

class PdfController extends Controller
{
    public function invoice(Request $request)
    {
        /*
        |------------------------------------------------------------------
        | Resultado de beneficios
        |-----------------------------------------------
        */
        $respuesta=array();
        $respuesta[6] = $request->resProspera;
        $respuesta[8] = $request->resInapam;
        $respuesta[5] = $request->resLiconsa;
        $respuesta[13] = $request->resPpam;
        $respuesta[2] = $request->resPei;
        $respuesta[4] = $request->resDiconsa;
        $respuesta[1] = $request->resImjuve;
        $respuesta[14] = $request->resPet;
        $respuesta[11] = $request->resFonart;
        $respuesta[15] = $request->resPaja;
        $respuesta[7] = $request->resPaimef;
        $respuesta[12] = $request->res3x1;
        $respuesta[10] = $request->resComedores;
        $respuesta[16] = $request->resConadis;
        $resProspera=$respuesta[6];
        $resInapam=$respuesta[8];
        $resLiconsa=$respuesta[5];
        $resPpam=$respuesta[13];
        $resPei=$respuesta[2];
        $resDiconsa=$respuesta[4];
        $resImjuve=$respuesta[1];
        $resPet=$respuesta[14];
        $resFonart=$respuesta[11];
        $resPaja=$respuesta[15];
        $resPaimef=$respuesta[7];
        $res3x1=$respuesta[12];
        $resComedores=$respuesta[10];
        $resConadis=$respuesta[16];
        $resCompromiso= $request->compromiso;

        if($resCompromiso == 'on'){
                     /*
            |-----------------------------------------------------------------
            |  Datos Beneficiario
            |----------------------------------------------------------
            */
            if($request->direccion != null){
                $direccion = $request->direccion;
            }
            if($request->direccion == null){
                $direccion = 0;
            }

            if($request->telefono != null){
                $telefono = $request->telefono;
            }
            if($request->telefono == null){
                $telefono = 0;
            }


            if($request->celular != null){
               $celular = $request->celular;
            }
            if($request->celular == null){
               $celular = 0;
            }

            if($request->nombre != null){
                $nombre = $request->nombre;
            }
            if($request->nombre == null){
                $nombre = 0;
            }

            if($request->email != null){
               $email = $request->email;
            }
            if($request->email == null){
               $email = 0;
            }

            if($request->cp != null){
               $cp = $request->cp;
            }
            if($request->cp == null){
               $cp = 0;
            }

            /*
            |-----------------------------------------------------------------
            |  Generar Folio
            |----------------------------------------------------------
            */
            $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            $numerodeletras=10;
            $folio = "";
            for($i=0;$i<$numerodeletras;$i++)
            {
                $folio .= substr($caracteres,rand(0,strlen($caracteres)),1);
            }

            if($request->nombre != null){
                $beneficiario = new Beneficiario;
                $beneficiario->folio = $folio;
                $beneficiario->nombre = $nombre;
                $beneficiario->telefono = $telefono;
                $beneficiario->celular = $celular;
                $beneficiario->email = $email;
                $beneficiario->cp = $cp;
                $beneficiario->direccion = $direccion;
                $beneficiario->entidadFederativa_id=9;
                $beneficiario->save();
                $item=$beneficiario->id;
                for($i=0; $i < 17; $i++)
                {
                    if(isset($respuesta[$i]))
                    {
                        if($respuesta[$i]==1)
                        {
                        $implicados= new Implicados;
                        $implicados->institucion_id=$i;
                        $implicados->ciudadanos_id=$item;
                            $implicados->save();
                            }
                    }
                }
            }

            $data = $this->getData();
            $date = date('Y-m-d');
            $invoice = $folio;
            $view = \View::make('user.pdf.invoice',
                        compact(
                            'data',
                            'date',
                            'invoice',
                            'nombre',
                            'telefono',
                            'resProspera',
                            'resInapam',
                            'resLiconsa',
                            'resPpam',
                            'resPei',
                            'resDiconsa',
                            'resImjuve',
                            'resPet',
                            'resFonart',
                            'resPaja',
                            'resPaimef',
                            'res3x1',
                            'resComedores',
                            'resConadis'
                        ))->render();

            $pdf = \App::make('dompdf.wrapper');
            #$pdf->set_paper("A4", "landscape");
            $pdf->loadHTML($view);
            return $pdf->stream('invoice');

        }
        if($resCompromiso != 'on'){
            return redirect()->back();
        }

    }

    public function getData()
    {
        $data =
        [
            'identidad'=>'Credencial para votar vigente, constancia de identidad con fotografía o de residencia, Pasaporte, Cartilla del Servicio Militar Nacional, Credencial del INAPAM, Cédula de Identidad Ciudadana, Cédula de Identidad Personal (sólo para menores de 18 años), Licencia de conducir, Credencial con fotografía de servicios médicos de una institución pública, Credencial con fotografía de jubilados(as) o pensionado(a)',
            'migratorios'=>'FM2 (Documento Migratorio de Inmigrante), FM3 (Documento Migratorio de No Inmigrante), Forma Migratoria de Inmigrante, Forma Migratoria de No Inmigrante, Forma Migratoria de Inmigrado',

            'PROSPERA' =>
            [
            'description'=>'Buscar que las familias que viven en situación de pobreza mejoren su calidad de vida, a través de acciones que amplíen sus capacidades en alimentación, salud y educación, y mejoren su acceso a otras dimensiones de bienestar.',
            'documentos'=>'Acta de nacimiento, CURP, Cualquiera de los documentos migratorios enlistados anteriormente'
            ],
            'PROSPERA' =>
            [
            'description'=>'Contribuir a la construcción de una sociedad incluyente mediante políticas que fomenten el ejercicio de los derechos de las personas adultas mayores y apoyar el desarrollo humano integral, a fin de mejorar sus niveles de bienestar y calidad de vida.',
            'documentos'=>'Acta de nacimiento, CURP, Cualquiera de los documentos migratorios enlistados anteriormente'
            ],
        ];



        return $data;
    }
}


