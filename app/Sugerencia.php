<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sugerencia extends Model
{
    //Use table own
    protected $table = 'sugerencias';

    protected $fillable = [
    	'id',
    	'sugerencia',
        'ciudadano_id',
    ];
}