<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Implicados extends Model
{
        protected $table = 'implicados';
       protected $fillable = [
        'institucion_id', 'ciudadanos_id', 'vigencia'
    ];

}
