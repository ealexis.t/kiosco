@extends('layouts.app')
@section('content')
<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/jquery.dataTables.js"></script>
<link rel="stylesheet" href="/assets/css/jquery.dataTables.min.css"/>
<div class="col-md-12" style="background:#fff;">
    <h4>Actualizados</h4>
    <hr>

    <table id="foo" class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th>Entidad</th>
                <th>Carencia</th>
                <th>Acción</th>
                <th>Mes</th>
                <th>Usuario</th>
                <th>Status</th>
                <th>Update</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($reportes as $reporte)
            @if($reporte->estado != null)
            <tr>
                <td>
                    @foreach($entidades as $entidad)
                        @if($entidad->id == $reporte->idEntidad)
                            {{$entidad->nombreEntidad}}
                        @endif
                    @endforeach
                </td>
                <td>
                    @foreach($carencias as $carencia)
                        @if($carencia->id == $reporte->idCarencia)
                            {{$carencia->nomCarencia}}
                        @endif
                    @endforeach
                </td>
                <td>
                    @foreach($acciones as $accion)
                        @if($accion->id == $reporte->idAccion)
                            {{$accion->acciones}}
                        @endif
                    @endforeach
                </td>
                <td>
                    @if($reporte->mes == 1)
                        Enero
                    @endif
                    @if($reporte->mes == 2)
                        Febrero
                    @endif
                    @if($reporte->mes == 3)
                        Marzo
                    @endif
                    @if($reporte->mes == 4)
                        Abril
                    @endif
                    @if($reporte->mes == 5)
                        Mayo
                    @endif
                    @if($reporte->mes == 6)
                        Junio
                    @endif
                    @if($reporte->mes == 7)
                        Julio
                    @endif
                    @if($reporte->mes == 8)
                        Agosto
                    @endif
                    @if($reporte->mes == 9)
                        Septiembre
                    @endif
                    @if($reporte->mes == 10)
                        Octubre
                    @endif
                    @if($reporte->mes == 11)
                        Noviembre
                    @endif
                    @if($reporte->mes == 12)
                        Diciembre
                    @endif
                </td>
                <td>
                    @if($reporte->tipoUser == '1')
                        Enlace
                    @endif
                    @if($reporte->tipoUser == '2')
                        Delegado
                    @endif
                </td>
                <td>{{$reporte->estado}}</td>
                <td>{{$reporte->updated_at}}</td>
                <td><a href="#" class="btn btn-sm bg-green-haze"data-toggle="modal" data-dismiss="modal"  data-target="#reporte{{$reporte->id}}">ver</a></td>
            </tr>

            <?php
            /*
            |--------------------------------------------------------------------------
            | Modal Reporte
            |-------------------------------------------------------------------
            */
            ?>
            <div class="modal fade" id="reporte{{$reporte->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Reporte {{$reporte->id}}</h4>
                        </div>
                        <div class="modal-body text-justify">
                            <div style="position:relative; margin-bottom:80px;">
                                <div style="position:relative; margin-bottom:130px;">
                                    <h4>Reporte:</h4>
                                    <div class="col-md-12" style="background:#f8f8f8;">
                                        <hr>
                                            {{$reporte->reporte}}
                                        <hr>
                                    </div>
                                </div>
                                <div style="position:relative; margin-bottom:130px;">
                                    <h4>Ipacto Mensual:</h4>
                                    <div class="col-md-12" style="background:#f8f8f8; font-size:17px;">
                                        <hr>
                                            <strong>{{$reporte->impactoMensual}}</strong>
                                        <hr>
                                    </div>
                                </div>
                             {{ Form::open(array('name' => 'f2','url' => 'statusReporte',  'method' => 'put',))}}
                                <h4>Status:</h4>
                                <input name="id" type="hidden" value="{{$reporte->id}}">
                                <input class="form-control" type="text" name="status" value="{{$reporte->estado}}">
                                <br>
                                <input type="submit" value="Actualizar" class="btn btn-success pull-left">
                                <br><br>
                            {{ Form::close() }}

                            <hr>
                            <div class="col-md-2 col-md-offset-10">
                                <a href="#"  class="btn btn-default pull-right " data-dismiss="modal">Cerrar</a>
                            </div>
                            <br><br><br>
                        </div>
                    </div>
                </div>
             </div>
            </div>

            <?php
                /*
                |--------------------------------------------------------------------------
                | Modal delete meta
                |-------------------------------------------------------------------


                <div class="modal fade" id="reporteDelete{{$reporte->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Eliminar {{$reporte->id}}</h4>
                            </div>
                            {{ Form::open(array('name' => 'f1','url' => 'eliminarReporte',  'method' => 'post', 'class'=>'form-horizontal row-fluid'))}}
                            <div class="modal-body text-center">
                                <strong>Eliminar {{$meta->titulo}}</strong>
                                <input name="id" type="hidden" value="{{$meta->id}}">
                                <input name="loteReporte" type="hidden" value="{{$meta->loteReporte}}">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary">Aceptar</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
           */
             ?>
             @endif
            @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript">
/*
**Ordena alfabéticamente de manera ascendente la tabla usuarios tomando la primera columna como referencia
*/
$("#foo").dataTable({
    order: [[1, "desc"]]
});
</script>
@endsection
@section('js-extras')
{{Html::script('assets/js/app.js')}}
@endsection