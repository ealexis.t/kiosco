    <!DOCTYPE html>
    <html lang="en">
    <!-- BEGIN HEAD -->
    <head>
    <meta charset="utf-8"/>
    <title>SEDESOL CONTIGO</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">


    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Global styles END -->

    <!-- Page level plugin styles BEGIN -->
    <link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
    <!-- Page level plugin styles END -->

        <link href="/assets/css/jquery.scroll.css" rel="stylesheet">
        <link href="/assets/css/jquery.scrollbar.css" rel="stylesheet">
    <link href="/assets/global/css/components.css" rel="stylesheet">
    <link href="/assets/frontend/onepage/css/style.css" rel="stylesheet">
    <link href="/assets/frontend/onepage/css/style-responsive.css" rel="stylesheet">
    <link href="/assets/frontend/onepage/css/themes/red.css" rel="stylesheet" id="style-color">
    <link href="/assets/frontend/onepage/css/custom.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
        <style>
        .itemm:hover h3{
            color: #83225E;
            font-size: 20px !important;
        }
            .back-text
            {
                background: url("/assets/image/ico_conadis-05.png")  fixed center !important;
            }
        body
    {
        background: url("/assets/image/new_design/t_g200px.png")  fixed center !important;

    }
            .blank-color
            {
                color:#fff;
            }
        .back-off
            {
                        background-color:#83225e;
            }
        .back-active
            {
                            background-color:#d7006d;
            }
        .back-static
            {
                height:800px;
            }

        .back-head
        {
             padding-top:10px;
            width:100%;
            height:100px;

        }
            .back-head img
            {

                margin-left:25px;
                height:80px;
            }
            .itemm img
            {
                width:50px !important;
                height:50px !important;
            }
            .back-white
            {
                padding-top:10px;
                background-color:#fff;

            }
            .menu-left h3{
                font-size:8px !important;
            }
            .title_active
            {
                color:#d7006d;
            }
            .list-item img

            {
                margin-top:10px;
                margin-right:50px;
                float:right;
            }
                .title-item-where 
        {
            padding-top:18px;
            border-radius:10px !important;
            height:100px;
            background-color:#83225e;
        }
        .title-item-where h3
        {
            color:#fff;
            font-size:18px !important;
        }
        .box-item
            {
                       border-radius:10px !important;
                height:120px;
                background-color:#eaeaea;
            }
            .box-item .title-box
            {
                        border-top-left-radius:10px !important;
                        border-top-right-radius:10px !important;
                color:#fff;
                height:40px;
            }
            .title-box h3
            {
                padding-top:8px;
                font-size:17px;
            }
            .direccion
            {
                font-size:16px;
            }
            .scroll-height
            {
                height:500px;
            }

            .tab-content
            {
                height:400px !important;
            }

            .actions ul
            {
                margin-right:10px;
                margin-top:-100px !important;
            }
            .color-off
            {
                color:#83225E;
            }
            .color-on
            {
                color:#D8006C;
            }
           .back-white2
           {
               padding-top:10px;
               width:450px;
               background-color:#d7006d;               position:relative;
               float:right;
               margin-right:10px;
               min-height: 100px;
               color:#fff;
               z-index:9999;
           }
                      .back-white2 img
           {
margin-left:10px;
           }
           .back-left
           {
               float:left;
           }
            .mary
            {
                display:none;
            }
            .ayuda
            {
                cursor:pointer;
            }
            
            .col-centered{
    float: none;
    margin: 0 auto;
}
        </style>

    @yield('style')

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}


    @yield('css-extras')
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
    </head>

    <body class="page-header-fixed page-quick-sidebar-over-content">

    @if(Session::has('message-error'))
        <div class="modal-success">
            <div class="message col-md-4 col-md-offset-4">
                <a class="pull-right" href="{{ URL('home') }}">Cerrar</a>
                <div class="text-center">
                    <img class="icon-mensaje" src="{{ asset('/assets/image/error.png') }}" alt="" height="100px" width="100px">
                </div>
                <hr style="border:solid #444 2px;">
                <p class="text-center">
                    {{ Session::get('message-error') }}
                </p>
            </div>
        </div>
        <script>
            function timer(){
                $(".modal-success").hide('fast');
                setInterval(redirect, 3000);
            }
            function redirect(){
                $("modal-success").css('display', 'none');
            }
            setInterval(timer, 2000);
        </script>
    @endif
    <!-- BEGIN HEADER -->
    <!-- END HEADER -->
    <div class="clearfix">
    </div>

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="back-head back-off">
                     <img src="{!! asset('/assets/image/new_design/logos.png') !!}" alt="" >
          <div class="back-white2 text-center mary">
                    <div class="row ">


                    <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2">
                        <img src="{!! asset('/assets/image/new_design/mari/mari-03.png') !!}" alt="" >
                    </div>
                    <div class="col-lg-10 col-md-10 col-xs-10 col-sm-10">
                        @yield('mensajemary')
                    </div>
                                        </div>
                </div>
        </div>
        <div class="row ">
            <div class="col-lg-1 col-md-1 col-xs-1 col-sm-1 back-white menu-left back-static text-center">
            @yield("atras")
              <a href="{{ URL('Sedesol') }}">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm ">
                    <div class="img">
                        <img src="{!! asset('/assets/image/new_design/menu-02.png') !!}" >
                    </div>
                  <h3>¿QUIÉNES SOMOS?</h3>
                </div>
            </a>

            <a href="{{ URL('Programas-Sociales') }}">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm">
                    <div class="img">
                                     <img src="{!! asset('/assets/image/new_design/menu-04.png') !!}" alt="">

                    </div>
                    <h3>PROGRAMAS SOCIALES</h3>
                </div>
            </a>
              <a href="{{ URL('Donde-Estamos') }}">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm">
                    <div class="img">
                <img src="{!! asset('/assets/image/new_design/menu-03.png') !!}" alt="" >
                    </div>
                    <h3>¿DÓNDE ESTAMOS?</h3>
                </div>
            </a>
            <a href="{{ URL('Beneficios') }}">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm">
                    <div class="img">
    <img src="{!! asset('/assets/image/new_design/menu-05.png') !!}" alt="" >

                    </div>
                    <h3>¿QUÉ BENEFICIOS PUEDO TENER?</h3>
                </div>
            </a>


            <a href="{{ URL('Tramites') }}">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm">
                    <div class="img">
    <img src="{!! asset('/assets/image/new_design/menu-06.png') !!}" alt="" >
                    </div>
                    <h3>TRÁMITES</h3>
                </div>
            </a>
            <a href="{{ URL('Quejas-Denuncias') }}">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm">
                    <div class="img">
    <img src="{!! asset('/assets/image/new_design/menu-07.png') !!}" alt="" >
                    </div>
                    <h3>ATENCIÓN AL BENEFICIARIO</h3>
                </div>
            </a>
            <a href="{{ URL('Noticias') }}">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm">
                    <div class="img">
    <img src="{!! asset('/assets/image/new_design/menu-08.png') !!}" alt="" >
                    </div>
                    <h3>NOTICIAS</h3>
                </div>
            </a>
            <a href="{{ URL('Contacto') }}">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm">
                    <div class="img">
    <img src="{!! asset('/assets/image/new_design/menu-09.png') !!}" alt="" >
                    </div>
                    <h3>BUZÓN DE SUGERENCIAS</h3>
                </div>
            </a>
            </div>
            <div class="col-lg-11 col-md-11 col-xs-11 col-sm-11  back-static " style="padding-right:0px;">
                     
                <div class=" scroll-height">
                        @yield('content')            
                </div>

            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="container">

            </div>
        </div>
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner">
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>

        <script src="/assets/js/app.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="/assets/js/jquery.scrolling.js" type="text/javascript"></script>
        <script src="/assets/js/jquery.mousewheel.js" type="text/javascript"></script>
        <script src="/assets/js/scrollproblem.js" type="text/javascript"></script>

    <!-- Core plugins END (For ALL pages) -->

    <!-- BEGIN RevolutionSlider -->

    <!-- END RevolutionSlider -->

    <!-- Core plugins BEGIN (required only for current page) -->
    <script src="/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <script src="/assets/global/plugins/jquery.easing.js"></script>
    <script src="/assets/global/plugins/jquery.parallax.js"></script>
    <script src="/assets/global/plugins/jquery.scrollTo.min.js"></script>
    <script src="/assets/frontend/onepage/scripts/jquery.nav.js"></script>
        <script src="/assets/js/jquery.scroll.js" type="text/javascript"></script>
    <!-- Core plugins END (required only for current page) -->

    <!-- Global js BEGIN -->
    <script src="/assets/frontend/onepage/scripts/layout.js" type="text/javascript"></script>

    <script>
        $(document).ready(function() {

                $('.scrollbar-inner').scrollbar();

                                $(".ayuda").click(function(){
                $(".mary").toggle();
                });

        });
        
        
    </script>

    @yield('js-extras')
    </body>
    </html>