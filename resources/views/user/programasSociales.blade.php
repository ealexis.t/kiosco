@extends('layouts.famsedesol')
<style>
body
{
        background: url("/assets/image/ppam.png")  fixed center !important;
}
.tresd
{
-moz-box-shadow: 0 0 5px 5px #AD0056;
-webkit-box-shadow: 0 0 5px 5px #AD0056;
box-shadow: 0 0 5px 5px #AD0056;
        border-radius:50px !important;
}
.back-header
{
  background-color:#AD0056;
}
.black-head
    {
        background: rgba(0,0,0,0.5);
        padding-bottom:20px;
        position:relative;
        top:-55px;
    }
</style>@section('atras')
        <a href="{{ URL('/') }}">
	        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm ">
	            <div class="img">
                    <img src="{!! asset('/assets/image/new_design/menu-01.png') !!}" >
	            </div>
	          <h3>ATRÁS</h3>
	        </div>
        </a>
@endsection
@section('mensajemary')
<p>Conoce que instituciones pertenecen a la SEDESOL</p>
@endsection
@section('title_item')
Programas Sociales
@endsection
@section('content')


<div class=" col-md-12 blank ">
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/comedores') }}">
        <img src="{!! asset('/assets/image/new_design/programas/comedores.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_centralizados_NORMAL-08.png') !!}" alt="" class="img-responsive">
	</a>
</div>    
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/conadis') }}">
        <img src="{!! asset('/assets/image/new_design/programas/conadis.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_sectorizados_NORMAL-01.png') !!}" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/diconsa') }}">
        <img src="{!! asset('/assets/image/new_design/programas/diconsa.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_sectorizados_NORMAL-04.png') !!}" alt="" class="img-responsive">
	</a>
</div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/fonart') }}">
        <img src="{!! asset('/assets/image/new_design/programas/fonart.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_sectorizados_NORMAL-03.png') !!}" alt="" class="img-responsive">
	</a>

</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/imjuve') }}">
        <img src="{!! asset('/assets/image/new_design/programas/imjuve.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_sectorizados_NORMAL-09.png') !!}" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/inaes') }}">
        <img src="{!! asset('/assets/image/new_design/programas/inaes.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_sectorizados_NORMAL-02.png') !!}" alt="" class="img-responsive">
	</a>
</div>
    
    
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/inapam') }}">
        <img src="{!! asset('/assets/image/new_design/programas/inapam.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_sectorizados_NORMAL-08.png') !!}" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/coinversion') }}">
        <img src="{!! asset('/assets/image/new_design/programas/coinversion.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/coinversion.png') !!}" alt="" class="img-responsive">
	</a>
</div>
    
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/paimef') }}">
        <img src="{!! asset('/assets/image/new_design/programas/paimef.png') !!}" alt="" class="img-responsive">
 		<img src="{!! asset('/assets/image/programas-sociales/paimef.png') !!}" alt="" class="img-responsive">
	</a>
</div>

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/liconsa') }}">
        <img src="{!! asset('/assets/image/new_design/programas/liconsa.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_sectorizados_NORMAL-05.png') !!}" alt="" class="img-responsive">
	</a>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
 	<a href="{{ URL('Programas-Sociales/paja') }}">
        <img src="{!! asset('/assets/image/new_design/programas/paja.png') !!}" alt="" class="img-responsive">
 		<img src="{!! asset('/assets/image/programas-sociales/logos_centralizados_NORMAL-05.png') !!}" alt="" class="img-responsive">
 	</a>
</div>



<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/pei') }}">
        <img src="{!! asset('/assets/image/new_design/programas/pei.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_centralizados_NORMAL-01.png') !!}" alt="" class="img-responsive">
	</a>
</div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/pet') }}">
        
        <img src="{!! asset('/assets/image/new_design/programas/pet.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_centralizados_NORMAL-06.png') !!}" alt="" class="img-responsive">
	</a>
</div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
 	<a href="{{ URL('Programas-Sociales/pfes') }}">
        <img src="{!! asset('/assets/image/new_design/programas/pfes.png') !!}" alt="" class="img-responsive">
 		<img src="{!! asset('/assets/image/programas-sociales/PFES.png') !!}" alt="" class="img-responsive">
 	</a>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/ppam') }}">
        <img src="{!! asset('/assets/image/new_design/programas/ppam.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_centralizados_NORMAL-04.png') !!}" alt="" class="img-responsive">
	</a>
</div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/prospera') }}">
        <img src="{!! asset('/assets/image/new_design/programas/prospera.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_sectorizados_NORMAL-06.png') !!}" alt="" class="img-responsive">
	</a>
</div>

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/svjf') }}">
        <img src="{!! asset('/assets/image/new_design/programas/svjf.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_centralizados_NORMAL-03.png') !!}" alt="" class="img-responsive">
	</a>
</div>



<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
	<a href="{{ URL('Programas-Sociales/tres') }}">
        <img src="{!! asset('/assets/image/new_design/programas/3x1.png') !!}" alt="" class="img-responsive">
		<img src="{!! asset('/assets/image/programas-sociales/logos_centralizados_NORMAL-07.png') !!}" alt="" class="img-responsive">
	</a>
</div>

</div>
@endsection
@section('modals')
@endsection
@section('js-extras')

@endsection