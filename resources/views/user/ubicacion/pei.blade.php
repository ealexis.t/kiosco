@extends('layouts.famsedesol')
    @section('style')
    <style>


        .back{
          background: #fff;
          padding: 40px;
          padding-bottom: 20px;
        }
     .black-head
        {
            background: rgba(0,0,0,0.5);
            padding-bottom:20px;
            position:relative;
            top:-55px;
        }
    </style>

 @endsection
@section('atras')
        <a href="{{ URL('Donde-Estamos') }}">
	        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm ">
	            <div class="img">
                    <img src="{!! asset('/assets/image/new_design/menu-01.png') !!}" >
	            </div>
	          <h3>ATRÁS</h3>
	        </div>
        </a>

@endsection
@section('title_item')
¿Dónde estamos?
@endsection
    @section('content')


   <div class="row blank">


        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
      <div id="map"></div>
      </div>
     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
         <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <img src="{!! asset('/assets/image/new_design/ubicacion/estancias1.png') !!}" >
         </div>
         <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <div class="title-item-where">
                    <h3>Estancias Infantiles</h3>
                </div>
             </div>
         <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <div class="box-item">
                    <div class="title-box back-active">
                        <h3>Usted se encuentra en:</h3>
                    </div>
                    <p class="direccion">Paseo de La Reforma 116, Cuauhtémoc, Juárez, 06600 Ciudad de México, D.F.</p>
                </div>
            </div>
     </div>
           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
               <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
                   <img src="{!! asset('/assets/image/new_design/bot3.png') !!}" >
               </div>
               <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-center">
            <label>Desplaza el círculo morado para ir desplegando más ubicaciones  </label>
            <input type=range min=1 max=20 value=1 step=1 id="range">
               </div>
                              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
                   <img src="{!! asset('/assets/image/new_design/bot4.png') !!}" >
               </div>
    </div>
      </div>


        <input type="hidden" name="ep" id="ep">

    @endsection
    @section('modals')

    @endsection
    @section('js-extras')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
     <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script type="text/javascript">
    var markers= []; var poly, geodesicPoly;
    var map;


    function initMap() {

               var center = {
        url: '/assets/image/kiosco.png',
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(40, 40),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 40)
      };


      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 19.2781257, lng: -99.6503582},
        zoom: 12
      });
                  var minZoomLevel = 8;
                 google.maps.event.addListener(map, 'zoom_changed', function () {
     if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
 });

                                          var item = {
        url: '/assets/image/new_design/ubicacion/estancias1.png',
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(40, 40),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 40)
      };
        
                      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 19.4288485, lng: -99.154042},
        zoom: 12
      });
      markers[0] = new google.maps.Marker({
        map: map,
        icon: center,
        position: {lat: 19.4288485, lng: -99.154042}
      });

        
        markers[0].addListener('click', function()
                              {
            $(".direccion").text("Calle Lucerna 24, Cuauhtémoc, Juarez, 06600 Ciudad de México");
            $(".responsable").text("Edmundo Rafal Ranero Barrera");
            $(".telefono").text("722 212 2296");
            $(".horarios").text("Sin registro");
        });
          var id = {
                  "id": 2
                };
                var data= $.param(id);
               $.ajax({
                url: 'http://172.25.11.64:9090/gsp',
                type:"GET",
                data: data,
                success: function(locat) {
                                var infowindow = new google.maps.InfoWindow;
                                var  i;
                                var locations= locat["data"];
                                for (var i = 0; i < locations.length; i++) {
                                    marker = new google.maps.Marker({
                                         position: new google.maps.LatLng(locations[i]["latitud"], locations[i]["longitud"]),
                                        icon: item,
                                         map: map
                                    });
                                    markers.push(marker);
                                    google.maps.event.addListener(markers[i+1], 'click', (function(marker, i) {
                                         return function() {
                                            
                                             document.getElementById('ep').value=locations[i]["id"];

                                             callgoal($("#ep").val());

                                         }
                                    })(marker, i));
                                }
                    

                        }
                });

/*
      google.maps.event.addListener(markers[0], 'position_changed', update);
      google.maps.event.addListener(markers[1], 'position_changed', update);
      poly = new google.maps.Polyline({
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        map: map,
      });

      geodesicPoly = new google.maps.Polyline({
        strokeColor: '#CC0099',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        geodesic: true,
        map: map
      });
    */
      //update();
    }
        
   

  function update() {
        var metros=$("#range").val()/2 * 1000;
                  
        $("#distancia").text(metros);
        var path = [];
        for (var i = 0; i < markers.length; i++) {
            path[i]=markers[i].getPosition();
            
        }
        for(i=1; i < path.length; i++)
        {
            distance = google.maps.geometry.spherical.computeDistanceBetween(path[0],path[i]);

            if(distance > metros)
            {
                console.log("i:"+i+" se va");
               markers[i].setVisible(false);
            }
            else{
                console.log("i:"+i+" se queda "+distance);
                 markers[i].setVisible(true);
            }
        }
    }
    $(document).on("change","#range",function()
    {
       update();
    }); 
        </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBn5blIIcG0LCtdS9uslDnpGHSamFPDeUI&libraries=geometry&callback=initMap&v=3"></script>
    @endsection 
@section('mensajemary')
<p> Selecciona cualquier punto marcado dentro del mapa para conocer su dirección. Si deseas conocer algún otro punto, desplaza hacia la derecha desde la barra inferior, el punto morado.
<img src="{!! asset('/assets/image/new_design/slider.png') !!}" style="width:100px; height:20px;">
</p>
@endsection