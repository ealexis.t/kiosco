@extends('layouts.famsedesol')
<style>

.tresd
{
-moz-box-shadow: 0 0 5px 5px #AD0056;
-webkit-box-shadow: 0 0 5px 5px #AD0056;
box-shadow: 0 0 5px 5px #AD0056;
        border-radius:50px !important;
}
.back-header
{
  background-color:#AD0056;
}
.black-head
    {
        background: rgba(0,0,0,0.5);
        padding-bottom:20px;
        position:relative;
        top:-55px;
    }

</style>
@section('atras')
        <a href="{{ URL('/') }}">
	        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm ">
	            <div class="img">
                    <img src="{!! asset('/assets/image/new_design/menu-01.png') !!}" >
	            </div>
	          <h3>ATRÁS</h3>
	        </div>
        </a>
@endsection
@section('mensajemary')
<p>Si te interesa conocer la ubicación de algún programa en específico selecciona los botones marcados con <img src="{!! asset('/assets/image/new_design/ubicacion/delegacion.png') !!}" style="background-color:#fff;">
</p>
@endsection
@section('title_item')
¿Dónde estamos?
@endsection
@section('content')

<div class="row blank ">
      <div class="team-block content content-center margin-bottom-40" id="team">
    <div class="col-md-12 col-xs-12">
        <a href="{{ URL('Donde-Estamos/delegaciones') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    <img src="{!! asset('/assets/image/new_design/ubicacion/delegacion.png') !!}" >
				</div>
                <div class="title-item-where">
				<h3>Delegaciones</h3>
                </div>
	        </div>
        </a>
       <a href="{{ URL('Donde-Estamos/fonart') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    <img src="{!! asset('/assets/image/new_design/ubicacion/fonart.png') !!}" >
				</div>
                <div class="title-item-where">
				<h3>Tiendas FONART</h3>
                                    </div>
	        </div>
        </a>


         <a href="{{ URL('Donde-Estamos/pei') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    <img src="{!! asset('/assets/image/new_design/ubicacion/estancias1.png') !!}" >
				</div>
                <div class="title-item-where">
                 <h3>Estancias Infantiles</h3>
                </div>

	        </div>
        </a>


         <a href="{{ URL('Donde-Estamos/diconsa') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    <img src="{!! asset('/assets/image/new_design/ubicacion/diconsa.png') !!}" >
				</div>
                <div class="title-item-where">
				<h3>Tiendas DICONSA</h3>
	           </div>
                                </div>
        </a>

    </div>
    </div>
</div>


<div class="row blank">
      <div class="team-block content content-center margin-bottom-40" id="team">


    <div class="col-md-12 col-xs-12">
                                   <a href="{{ URL('Donde-Estamos/imjuve') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    <img src="{!! asset('/assets/image/new_design/ubicacion/poder.png') !!}" >
				</div>
                <div class="title-item-where">
                    <h3>Centros Poder Joven</h3>
                                    </div>
	        </div>
        </a>
        <a href="{{ URL('Donde-Estamos/liconsa') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    <img src="{!! asset('/assets/image/new_design/ubicacion/liconsa.png') !!}" >
				</div>
                <div class="title-item-where">
				<h3>Lecherías LICONSA</h3>
                                    </div>
	        </div>
        </a>

       <a href="{{ URL('Donde-Estamos/prospera') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    <img src="{!! asset('/assets/image/new_design/ubicacion/delprospera.png') !!}" >
				</div>
                <div class="title-item-where">
				<h3>Delegaciones PROSPERA </h3>
                                    </div>
	        </div>
        </a>




         <a href="{{ URL('Donde-Estamos/inapam') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    <img src="{!! asset('/assets/image/new_design/ubicacion/inapam.png') !!}" >
				</div>
                <div class="title-item-where">
				<h3>Clubes INAPAM </h3>
                                    </div>
	        </div>
        </a>


    </div>
    </div>
</div>

<div class="row blank">
      <div class="team-block content content-center margin-bottom-40" id="team">
    <div class="col-md-12 col-xs-12">
        <a href="{{ URL('Donde-Estamos/paimef') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                 <img src="{!! asset('/assets/image/new_design/ubicacion/paimef.png') !!}" >    
				</div>
                <div class="title-item-where">
				<h3>Centros de Apoyo a Mujeres en Entidades Federativas </h3>
                                    </div>
	        </div>
        </a>
       <a href="{{ URL('Donde-Estamos/organizaciones') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    <img src="{!! asset('/assets/image/new_design/ubicacion/organizaciones.png') !!}" >
				</div>
                <div class="title-item-where">
				<h3>Organizaciones de la Sociedad Civil</h3>
                                    </div>
	        </div>
        </a>


         <a href="{{ URL('Donde-Estamos/casas') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">  
               <img src="{!! asset('/assets/image/new_design/ubicacion/casasart.png') !!}" >
				</div>
                <div class="title-item-where">
                    <h3>Casas Artesanales </h3>
                </div>
	        </div>
        </a>


         <a href="{{ URL('Donde-Estamos/comedores') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">  
                     <img src="{!! asset('/assets/image/new_design/ubicacion/comedores.png') !!}" >
				</div>
                <div class="title-item-where">
				    <h3>Comedores Comunitarios</h3>
                </div>
	        </div>
        </a>

    </div>
    </div>
</div>


  <!-- Team block END -->
@endsection
@section('modals')

@endsection
@section('js-extras')

@endsection