     @extends('layouts.programas')@section('atras')
        <a href="{{ URL('/Programas-Sociales') }}">
	        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm ">
	            <div class="img">
                    <img src="{!! asset('/assets/image/new_design/menu-01.png') !!}" >
	            </div>
	          <h3>ATRÁS</h3>
	        </div>
        </a>
@endsection
@section('mensajemary')
<p>Conoce lo que este programa te ofrece. Selecciona algunas de las sguientes pestañas (Función, ¿A quiénes apoya?, ¿Cómo apoya? y ¿Cómo recibir el apoyo?)
</p>
@endsection
@section('content')

<div class="back-text">
    <div class="row head">
        <div class="col-md-12 blank-color">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <img src="{!! asset('/assets/image/new_design/programas/prospera.png') !!}" width="150px" height="150px">
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-right pt20 ayuda"  >
            <h1>PROSPERA</h1>
                <p>
                Programa de Inclusión Social
                </p>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 list-item itemm">
                <img src="{!! asset('/assets/image/new_design/bot2.png') !!}" alt="" >
                </div>

        </div>
    </div>
</div>
<div class="row blank scrollbar-inner scroll-height col-centered col-xs-10 col-sm-10 col-md-10 col-lg-10">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-center">
          <p class="margin-bottom-10">
            Buscar que las familias que viven en situación de
            pobreza mejoren su calidad de vida, a través de
            acciones que amplíen sus capacidades en
            alimentación, salud y educación, y mejoren su
            acceso a otras dimensiones de bienestar.
          </p>
        </div>
        <div class="text-center">
          @include('partials.imgProgramas.prospera.prospera1',array())
        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-center">
          <p>
            Familias que viven en situación de pobreza, dando
            prioridad a hogares con integrantes menores de 22
            años y mujeres en edad reproductiva.
          </p>
        </div>
        <div class="text-center">
          @include('partials.imgProgramas.prospera.prospera2',array())
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">

        <div class="col-md-12 text-left">
            Considerando la cobertura y capacidad de atención de los servicios de educación y salud, el Programa cuenta con dos esquemas de apoyos:
            <ol>
               <li>
                  Esquema de Apoyos con Corresponsabilidad: Los beneficiarios
                  reciben cada dos meses los apoyos
                  monetarios correspondientes, en función del
                  cumplimiento de sus compromisos en
                  educación y salud.
                  <br>
                  Las familias contempladas dentro de este
                  esquema reciben los apoyos
                  correspondientes a los componentes:
                  educativo, salud, alimentario y vinculación.
               </li>
               <li>Esquema de Apoyos sin Corresponsabilidad: Los beneficiarios
                  reciben cada dos meses los apoyos
                  monetarios correspondientes, sin estar
                  sujetos al cumplimiento de compromisos en
                  educación y salud.
                  <br>
                  Las familias contempladas dentro de este
                  esquema podrán recibir los apoyos
                  correspondientes a los componentes:
                  alimentario y vinculación.
               </li>
            </ol>
            <br><br>
            <p>
            Los apoyos monetarios que recibe las familias beneficiarias varían conforme al número de integrantes menores de 9 años, y en su caso, al de

            becarios (as) y grado escolar que cursen, así como el número de personas adultas mayores incorporadas en el hogar.
            <br><br>

            El monto de todos los apoyos monetarios se actualiza semestralmente, de acuerdo a la disponibilidad presupuestal y a los indicadores que
            maneja Consejo Nacional de Evaluación de la Política de Desarrollo Social (CONEVAL).
            <br><br>
            El monto mensual de los apoyos monetarios se difunde a la población beneficiaria y se publica en la página institucional en internet www.prospera.gob.mx
            <br>
            (https://www.prospera.gob.mx/swb/es/PROSPERA2015/Monto_de_Apoyos)
            </p>
      </div>
        <div class="text-center">
          @include('partials.imgProgramas.prospera.prospera3',array())
        </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">

        <div class="col-md-12 text-justify">
        <h4> Requisitos generales:</h4>
          <ol class="text-left listado">
            <li>
              <p>
                  Recibir en el domicilio del interesado la visita
                  del personal operativo de PROSPERA
                  (debidamente identificado), para el levantamiento
                  de información socioeconómica a través del
                  cuestionario CUIS-ENCASEH, en el cual se
                  registran los datos sobre las características del
                  hogar y de las condiciones socioeconómicas y
                  demográficas de todos sus integrantes.
              </p>
            </li>
            <li>
              <p>
                Calificar como familia elegible para PROSPERA.
                Las familias elegibles son aquellas cuyo ingreso
                mensual neto, por persona, sea menor al valor de la
                canasta básica alimentaria.
              </p>
            </li>
            <li>
              <p>
                Presentar original y copia de alguno de
                los siguientes documentos del titular:
              </p>
              <ul>
                <li><p>Credencial para votar vigente</p></li>
                <li><p>Constancia de identidad con fotografía o de residencia.</p></li>
                <li><p>Pasaporte</p></li>
                <li><p>Cartilla del Servicio Militar Nacional</p></li>
                <li><p>Credencial del INAPAM</p></li>
                <li><p>Cédula de Identidad Ciudadana</p></li>
                <li><p>Cédula de Identidad Personal (sólo para menores de 18 años).</p></li>
                <li><p>Documentos migratorios:</p>
                    <ul>
                      <li><p>FM2 (Documento Migratorio de Inmigrante)</p></li>
                      <li><p>FM3 (Documento Migratorio de No Inmigrante)</p></li>
                      <li><p>Forma Migratoria de Inmigrante</p></li>
                      <li><p>Forma Migratoria de No Inmigrante</p></li>
                      <li><p>Forma Migratoria de Inmigrado</p></li>
                    </ul>
                </li>
                <li><p>Licencia de conducir.</p></li>
                <li><p>
                  Credencial con fotografía de
                  servicios médicos de una
                  institución pública.
                </p></li>
                <li><p>
                  Credencial con fotografía de
                  jubilados(as) o pensionado(a)
                </p></li>
              </ul>
            </li>
            <li>
              <p>
                El resto de los integrantes de la familia
                deberán presentar original y copia de
                alguno de los siguientes documentos:
              </p>
              <ul>
                <li><p>Acta de nacimiento</p></li>
                <li><p>CURP</p></li>
                <li><p>Cualquiera de los documentos migratorios enlistados anteriormente</p></li>
              </ul>
            </li>
          </ol>

        </div>
        <div class="text-center">
          @include('partials.imgProgramas.prospera.prospera4',array())
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection