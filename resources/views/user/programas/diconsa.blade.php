    @extends('layouts.programas')@section('atras')
        <a href="{{ URL('/Programas-Sociales') }}">
	        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm ">
	            <div class="img">
                    <img src="{!! asset('/assets/image/new_design/menu-01.png') !!}" >
	            </div>
	          <h3>ATRÁS</h3>
	        </div>
        </a>
@endsection
@section('mensajemary')
<p>Conoce lo que este programa te ofrece. Selecciona algunas de las sguientes pestañas (Función, ¿A quiénes apoya?, ¿Cómo apoya? y ¿Cómo recibir el apoyo?)
</p>
@endsection
@section('content')

<div class="back-text">
    <div class="row head">
        <div class="col-md-12 blank-color">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                

              <img src="{!! asset('/assets/image/new_design/programas/diconsa.png') !!}" width="150px" height="150px">
            </div>

                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-right pt20"  >
            <h1>DICONSA</h1>
                <p>
                    Abasto Social
                </p>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 list-item itemm ayuda">
                <img src="{!! asset('/assets/image/new_design/bot2.png') !!}" alt="" >
                </div>

        </div>
    </div>
</div>
<div class="row blank scrollbar-inner scroll-height col-centered col-xs-10 col-sm-10 col-md-10 col-lg-10">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-center">
          <p class="margin-bottom-10">
            Contribuir a fortalecer el
            cumplimiento efectivo del derecho social a la
            alimentación, facilitando el acceso físico o
            económico a los productos alimenticios para la
            población que habita en las localidades de alta o
            muy alta marginación.
          </p>
          <div class="text-center">
            @include('partials.imgProgramas.diconsa.diconsa1',array())
          </div>

        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-center">
          <p>
            Familias que vivan en localidades de alta o muy
            alta marginación que tengan entre 200 y 14,999
            habitantes, y que no tengan un abasto de productos
            básicos suficientes y adecuados.
          </p>
          <div class="text-center">
            @include('partials.imgProgramas.diconsa.diconsa2',array())
          </div>
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">
        <div class="col-md-12 text-justify">
           <p class="margin-bottom-10">
            Consiste en proporcionar en sus
            Puntos de Venta el servicio de abasto de
            productos básicos y complementarios de calidad.
            En el caso de los productos alimenticios, se
            procura que además tengan un alto valor nutritivo.
            <br><br>
            <strong>NOTA:</strong>
            <br>
            Se busca que el
            Margen de Ahorro otorgado a las personas
            beneficiarias, a través del precio de la Canasta
            Básica Diconsa distribuida en los Puntos de Venta,
            sea de por lo menos 15%, considerando el precio
            de dicha canasta en las tiendas privadas del
            mercado local.
          </p>
          <div class="text-center">
            @include('partials.imgProgramas.diconsa.diconsa3',array())
          </div>
      </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">
        <div class="col-md-12 text-justify">
          <h4>Clientes DICONSA</h4>
          <ul class="text-left listado">
            <li>
                <p>
                Asistir a una tienda DICONSA y pagar los
                productos o servicios.
                </p>
            </li>
          <h4>Apertura de una tienda DICONSA</h4>
          <ul class="text-left listado">
            <li>
                <p>
                Llenar el formato de Solicitud de Apertura de Tienda de manera:
                </p>
                <ul>
                  <li><p>
                    <strong>Electrónica</strong>; a través de la página del
                    Catálogo Nacional de Trámites y
                    Servicios del Estado (CNTSE) disponible
                    en www.gob.mx
                  </p></li>
                  <li><p>
                    <strong>En caso de no contar con acceso a internet</strong>, una o
                    un representante de la comunidad interesada
                    podrá entregar al personal de Diconsa la Solicitud
                    de Apertura de Tienda (anexo 5) avalada por la
                    firma de al menos quince jefes o jefas de familia.
                  </p></li>
                </ul>
            </li>
            <div class="text-center">
            @include('partials.imgProgramas.diconsa.diconsa4',array())
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection