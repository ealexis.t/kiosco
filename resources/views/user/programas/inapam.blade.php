    @extends('layouts.programas')@section('atras')
        <a href="{{ URL('/Programas-Sociales') }}">
	        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm ">
	            <div class="img">
                    <img src="{!! asset('/assets/image/new_design/menu-01.png') !!}" >
	            </div>
	          <h3>ATRÁS</h3>
	        </div>
        </a>
@endsection
@section('mensajemary')
<p>Conoce lo que este programa te ofrece. Selecciona algunas de las sguientes pestañas (Función, ¿A quiénes apoya?, ¿Cómo apoya? y ¿Cómo recibir el apoyo?)
</p>
@endsection
@section('content')
<div class="back-text">
    <div class="row head">
        <div class="col-md-12 blank-color">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <img src="{!! asset('/assets/image/new_design/programas/inapam.png') !!}" width="150px" height="150px">
            </div>


                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-right pt20"  >
            <h1>INAPAM</h1>
                <p>Instituto Nacional de las Personas Adultas Mayores</p>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 list-item itemm ayuda">
                <img src="{!! asset('/assets/image/new_design/bot2.png') !!}" alt="" >
                </div>
        </div>
    </div>
</div>
<div class="row blank scrollbar-inner scroll-height col-centered col-xs-10 col-sm-10 col-md-10 col-lg-10">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
        <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-center">
          <p class="margin-bottom-10">Contribuir a la construcción de una sociedad incluyente mediante políticas que fomenten el ejercicio de los derechos de las personas adultas mayores y apoyar el desarrollo humano integral, a fin de mejorar sus niveles de bienestar y calidad de vida.</p>
            <div class="text-center">
            @include('partials.imgProgramas.inapam.inapam1',array())
            </div>
        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-center">
          <p>A todas las personas que tengan 60 años cumplidos y más, que se encuentren domiciliadas o en tránsito en el territorio nacional.</p>
            <div class="text-center">
            @include('partials.imgProgramas.inapam.inapam2',array())
            </div>
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">
        <div class="col-md-12 text-center">
            <ul class="text-left listado">
          <li><p>Proporciona la tarjeta INAPAM, que identifica a la persona adulta mayor con su grupo de edad y le permite acceder a beneficios y descuentos en bienes y servicios en establecimientos a nivel nacional.</p></li>
          <li><p>Promueve actividades educativas, culturales, deportivas, sociales y recreativas, en espacios comunitarios (clubes), que le permite a la persona adulta mayor permanecer en la familia y su comunidad.</p></li>
          <li><p>Fomenta acciones dirigidas a prevenir y mejorar las condiciones de vida de las personas adultas mayores, a través de cursos, talleres y pláticas sobre educación para la salud.</p></li>
          <li><p>Gestiona empleos remunerados, así como actividades voluntarias que generen ingresos a las Personas Mayores conforme a su oficio, habilidad o profesión.</p></li>
          <li><p>Impulsa la actividad física para fomentar el cuidado de la salud, el deporte y una cultura de la vejez.</p></li>
          </ul>
          <div class="text-center">
          @include('partials.imgProgramas.inapam.inapam3',array())
          </div>
      </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">
        <div class="col-md-12 text-center">
            <ul class="text-left listado">
            <li><p>Las personas adultas mayores que solicitan la Tarjeta INAPAM deberán cubrir los siguientes requisitos:</p> <p>a) Comprobar su identidad</p>
<p>
b) Comprobar su edad (tener 60 años cumplidos o más)
                </p>
                <p>
c) Comprobar su domicilio
                </p>
                <p>
<br>
Adicionalmente se necesitan 2 fotografías tamaño infantil. Esos requisitos podrán cubrirse presentando la

credencial para votar vigente (INE)<br><br></p></li>
            <li><p>Las personas mayores que deseen de los servicios que proporciona el Instituto, sólo deberá presentar su tarjeta INAPAM</p></li>
            </ul>
            <div class="text-center">
            @include('partials.imgProgramas.inapam.inapam4',array())
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection