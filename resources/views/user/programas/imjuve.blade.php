     @extends('layouts.programas')@section('atras')
        <a href="{{ URL('/Programas-Sociales') }}">
	        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm ">
	            <div class="img">
                    <img src="{!! asset('/assets/image/new_design/menu-01.png') !!}" >
	            </div>
	          <h3>ATRÁS</h3>
	        </div>
        </a>
@endsection
@section('mensajemary')
<p>Conoce lo que este programa te ofrece. Selecciona algunas de las sguientes pestañas (Función, ¿A quiénes apoya?, ¿Cómo apoya? y ¿Cómo recibir el apoyo?)
</p>
@endsection
@section('content')
<div class="back-text">
    <div class="row head">
        <div class="col-md-12 blank-color ">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <img src="{!! asset('/assets/image/new_design/programas/imjuve.png') !!}" width="150px" height="150px">
            </div>

                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-right pt20"  >
            <h1>IMJUVE</h1>
                <p>Instituto Mexicano de la Juventud</p>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 list-item itemm ayuda">
                <img src="{!! asset('/assets/image/new_design/bot2.png') !!}" alt="" >
                </div>
        </div>
    </div>
</div>
<div class="row blank scrollbar-inner scroll-height  col-centered col-xs-10 col-sm-10 col-md-10 col-lg-10">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
        <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-center">
          Impulsar y promover el desarrollo integral de las y los
          jóvenes, a través del diseño, coordinación, articulación
          y monitoreo de la política nacional de la
          juventud.
        </div>
        <div class="text-center">
          @include('partials.imgProgramas.imjuve.imjuve1',array())
        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-center">
        A mexicanas y mexicanos jóvenes entre los 12 y 29 años de edad.
        </div>
        <div class="text-center">
          @include('partials.imgProgramas.imjuve.imjuve2',array())
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">
        <div class="col-md-12 text-justify">
          <ol class="listado">
            <li>
                          <p>
                Brinda apoyos directos a las y los jóvenes, a
                través de concursos y/o premios, para fortalecer
                su acceso a la educación, participación social,
                trabajo y salud que les permitan consolidar su
                incorporación equitativa en los procesos de
                desarrollo.
                </p>
            </li>

            <li>          <p>Subsidia programas para jóvenes.</p>
       
            </li>
            <li>
              <p>Provee apoyos económicos directos a
              instancias estatales y municipales de juventud
              e instituciones públicas de educación superior o
              media superior, para consolidar la
              incorporación equitativa de las personas
              jóvenes en los procesos de desarrollo.</p>
            </li>
          </ol>
        </div>
        <div class="text-center">
          @include('partials.imgProgramas.imjuve.imjuve3',array())
        </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">
        <div class="col-md-12 text-justify">
           <ol class="listado">
            <li>
                    <p>
                E016: Cumplir con los requisitos estipulados en las
                políticas de operación vigentes del programa y, en
                su caso, con los requisitos especificados en cada
                convocatoria y ser seleccionado por el respectivo
                comité dictaminador. Las convocatorias se publican
                en el sitio web del IMJUVE: www.imjuventud.gob.mx
                        </p>
            </li>
            <li>
                <p>
              U008: Cumplir con los requisitos estipulados en la
              convocatoria específica de cada tipo de apoyo y ser
              seleccionado por parte del respectivo comité
              dictaminador. Las convocatorias son anuales y se
              publican en el sitio web del IMJUVE:
              www.imjuventud.gob.mx
                    </p>
            </li>
          </ol>
        </div>
        <div class="text-center">
          @include('partials.imgProgramas.imjuve.imjuve4',array())
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection