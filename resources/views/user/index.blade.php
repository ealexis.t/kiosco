@extends('layouts.app')
<style>
body
{
    background: url("/assets/image/new_design/t_g200px.png")  fixed center !important;

}
h3{
	font-size: 16px !important;
}

</style>
@section('content')
  <!-- Team block BEGIN -->



<div class="team-block content content-center margin-bottom-40" id="team">
    <div class="container blank tresd">
    <div class="row top-buffer">
            <h3>Bienvenido al Kiosco Digital de la SEDESOL</h3>
        </div>
      <div class="row ">
      	<a href="{{ URL('Sedesol') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item ">
	            <div class="img">
              @include('partials.menu.sedesol',array())

	            </div>
	          <h3>¿QUIÉNES SOMOS?</h3>
	        </div>
        </a>
        
        <a href="{{ URL('Programas-Sociales') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
            @include('partials.menu.programas',array())
				</div>
				<h3>PROGRAMAS SOCIALES</h3>
	        </div>
	    </a>
          <a href="{{ URL('Donde-Estamos') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
              @include('partials.menu.ubicacion',array())
				</div>
				<h3>¿DÓNDE ESTAMOS?</h3>
	        </div>
        </a>
	    <a href="{{ URL('Beneficios') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
	          	<div class="img">
@include('partials.menu.beneficio',array())

	          	</div>
	            <h3>¿QUÉ BENEFICIOS PUEDO TENER?</h3>
	        </div>
        </a>
                  
                           
        <a href="{{ URL('Tramites') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
	            <div class="img">
@include('partials.menu.tramites',array())
	            </div>
	            <h3>TRÁMITES</h3>
	        </div>
        </a>
        <a href="{{ URL('Quejas-Denuncias') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
	            <div class="img">
@include('partials.menu.atencion',array())
	            </div>
	            <h3>ATENCIÓN AL BENEFICIARIO</h3>
	        </div>
        </a>
        <a href="{{ URL('Noticias') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
	            <div class="img">
@include('partials.menu.noticias',array())
	            </div>
	            <h3>NOTICIAS</h3>
	        </div>
        </a>
        <a href="{{ URL('Contacto') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
	            <div class="img">
@include('partials.menu.buzon',array())
	            </div>
	            <h3>BUZÓN DE SUGERENCIAS</h3>
	        </div>
        </a>

      </div>
    </div>
  </div>
  <!-- Team block END -->
@endsection
@section('modals')

@endsection
@section('js-extras')

@endsection