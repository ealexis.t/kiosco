@extends('layouts.famsedesol')
<style>

.tresd
{
-moz-box-shadow: 0 0 5px 5px #AD0056;
-webkit-box-shadow: 0 0 5px 5px #AD0056;
box-shadow: 0 0 5px 5px #AD0056;
        border-radius:50px !important;
}
.back-header
{
  background-color:#AD0056;
}
.black-head
    {
        background: rgba(0,0,0,0.5);
        padding-bottom:20px;
        position:relative;
        top:-55px;
    }
</style>
@section('atras')
        <a href="{{ URL('/') }}">
	        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm ">
	            <div class="img">
                    <img src="{!! asset('/assets/image/new_design/menu-01.png') !!}" >
	            </div>
	          <h3>ATRÁS</h3>
	        </div>
        </a>
@endsection
@section('title_item')
Buzón de sugerencias
@endsection
@section('content')


<div class="row blank">

</div>
<div class="row blank">
    <div class="col-md-4 " style="padding-top:100px;">
        @include('partials.buzon.buzon',array())
    </div>
    <div class="col-md-6">
        @if(Session::has('message'))
            <h2>{{ Session::get('message') }}</h2>
        @endif

        <h4>Rellene el formulario con sus datos para ponernos en contacto con usted </h4>
        {{Form::open(array('name' => 'sugerencia', 'url' => 'sugerencia', 'method' => 'post', 'class'=>'',))}}
            <div class="form-group">
                <label>Nombre:</label>
                <input type="text" name="nombre" class="form-control" required>
            </div>
            <div class="form-group">
                <label>Correo electrónico:</label>
                <input type="text" name="email" class="form-control">
            </div>
            <div class="form-group">
                <label>Teléfono:</label>
                <input type="text" name="telefono" class="form-control" required >
            </div>
            <div class="form-group">
                <label>Asunto:</label>
                <textarea name="sugerencia" class="form-control" rows="3" required></textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-block btn-primary">Enviar</button>
            </div>
        {{ Form::close() }}
    </div>
</div>

@endsection
@section('modals')

@endsection
@section('js-extras')

@endsection

@section('mensajemary')
<p> Si tienes alguna duda o sugerencia, contáctanos a través del siguiente formulario.

</p>
@endsection

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title uppercase text-center" id="myModalLabel">AVISO IMPORTANTE</h2>
              </div>
              <div class="modal-body text-center">
                <p style="color:#83225e;">
                    Los datos han sido guardados con éxito
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
    </div>
</div>
