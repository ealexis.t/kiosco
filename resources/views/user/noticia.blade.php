@extends('layouts.famsedesol')

@section('css-extras')

<style>

.tresd
{
-moz-box-shadow: 0 0 5px 5px #AD0056;
-webkit-box-shadow: 0 0 5px 5px #AD0056;
box-shadow: 0 0 5px 5px #AD0056;
        border-radius:50px !important;
}
.back-header
{
  background-color:#AD0056;
}
.black-head
    {
        background: rgba(0,0,0,0.5);
        padding-top:20px;
        padding-bottom:20px;
        position:relative;
        top:-55px;

    }
</style>
@endsection
@section('atras')
        <a href="{{ URL('/') }}">
	        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 itemm ">
	            <div class="img">
                    <img src="{!! asset('/assets/image/new_design/menu-01.png') !!}" >
	            </div>
	          <h3>ATRÁS</h3>
	        </div>
        </a>
@endsection
@section('title_item')
Noticias
@endsection

@section('content')
<div class="carousel slide" data-ride="carousel" id="testimonials-block" style="width:710px;" >
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <!-- Carousel items -->
    <div class="item active">
      <img src="/assets/image/slideNoticias/1.png" alt="" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
    </div>
    <!-- Carousel items -->
    <div class="item">
      <img src="/assets/image/slideNoticias/2.png" alt="" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
    </div>
    <!-- Carousel items -->
    <div class="item">
      <img src="/assets/image/slideNoticias/3.png" alt="" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
    </div>
    <!-- Carousel items -->
    <div class="item">
      <img src="/assets/image/slideNoticias/4.png" alt="" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
    </div>
    <!-- Carousel items -->
    <div class="item">
      <img src="/assets/image/slideNoticias/5.png" alt="" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
    </div>
    <!-- Carousel items -->
    <div class="item">
      <img src="/assets/image/slideNoticias/6.png" alt="" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
    </div>
    <!-- Carousel items -->
    <div class="item">
      <img src="/assets/image/slideNoticias/7.png" alt="" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
    </div>
    <!-- Carousel items -->
    <div class="item">
      <img src="/assets/image/slideNoticias/8.png" alt="" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
    </div>
  </div>
  <!-- Indicators -->
  <ol class="carousel-indicators" style="background:#666;">
    <li data-target="#testimonials-block" data-slide-to="0" class="active"></li>
    <li data-target="#testimonials-block" data-slide-to="1" class=""></li>
    <li data-target="#testimonials-block" data-slide-to="2" class=""></li>
    <li data-target="#testimonials-block" data-slide-to="3" class=""></li>
    <li data-target="#testimonials-block" data-slide-to="4" class=""></li>
    <li data-target="#testimonials-block" data-slide-to="5" class=""></li>
    <li data-target="#testimonials-block" data-slide-to="6" class=""></li>
    <li data-target="#testimonials-block" data-slide-to="7" class=""></li>
  </ol>
</div>

@endsection
@section('modals')

@endsection
@section('js-extras')

@endsection

@section('mensajemary')
<p> La SEDESOL te mantiene informado, conoce los avances y metas cumplidas de la Secretaría.

</p>
@endsection